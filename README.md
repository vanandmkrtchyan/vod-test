## This is the whole task repo for VOD

### Installation steps 

- clone this repository
```
git clone https://bitbucket.org/vanandmkrtchyan/vod-test.git
```
- run this command
```
composer install
```
- run migrations
```
php artisan migrate
```
- serve the project
```
php artisan serve
```
- open in browser the address [http://localhost:8000](http://localhost:8000)
- register
- add companies
- add categories
- add pills